package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    public void Startback2(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void Iglink(View view) {
        Intent intend2 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/__aemharmonizer/?hl=th"));
        startActivity(intend2);
    }

    public void fblink(View view) {
        Intent intend3 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/profile.php?id=100001055668801"));
        startActivity(intend3);
    }

    public void Youtubelink(View view) {
        Intent intend4 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCJorpSAsySglS-NZ2Ir2CwQ?view_as=subscriber"));
        startActivity(intend4);
    }
}